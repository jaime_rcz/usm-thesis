#Plantilla para Memorias y Tesis, Departamento de Industrias, USM

Plantilla básica (en LaTeX y LyX) para las Memorias y Tesis del [Departamento de Industrias](http://www.industrias.usm.cl) de la [Universidad Técnica Federico Santa María](http://www.usm.cl), Chile.

Aunque generan (casi)[^1] el mismo resultado, las plantillas en LaTeX y LyX son diferentes; fueron realizadas por separado.

[^1]: LaTeX y Lyx no son idénticos porque LyX ocupa un paquete de geometría de página levente diferente.

##Requisitos

Esta plantilla requiere:

* Una versión reciente de LaTeX (2014 o posterior).
* El editor LyX (sólo para plantilla `*.lyx`).

##Uso

###LaTeX

La plantilla en LaTeX se encuentra en la carpeta del mismo nombre.

Para usar esta plantilla tan sólo debe:

* modificar los archivos de apoyo (portada, resumen, etc.) que desee,
* cambiar parámetros básicos como autor y fecha (sección CONFIGURACIÓN) en el documento maestro (`memoria.tex`); y,
* compilar este archivo (usando una consola LaTeX o su editor favorito):


		$ pdflatex memoria.tex
		$ bibtex memoria
		$ pdflatex memoria.tex
		$ pdflatex memoria.tex

**NOTA:** `pdflatex` debe ejecutarse (en consola) tres veces, como se indica y en   el orden mostrado para que latex pueda construir las Tablas de Contenidos y las   referencias cruzadas de la Bibliografía.

###LyX

La plantilla en LyX se encuentra en la carpeta del mismo nombre.

Para usar la plantilla en LyX, sólo debe editar los archivos que desee modificar. **No debe editar** el documento mastro (`memoria.lyx`).

Para compilar, abra este documento maestro (`memoria.lyx`) y podrá compilarlo en el Menú 'Ver' -> 'Ver [PDF (pdflatex)] (CRTL+r ó CMD+r en Mac).

##Plataformas Soportadas

Esta plantilla es independiente de la plataforma empleada (Windows, Mac o Linux).

**Nota:** la plantilla en LaTeX fue escrita usando una codificación de caracteres UTF-8, la que en ocasiones puede general conflictos en Windows cuando se agregan archivos con codificación distinta. Windows ocupa por defecto una codificación no compatible con UTF-8.

##Documentación

Revise los archivos de salida (`memoria.pdf`) de cada plantilla, LaTeX o Lyx, para más información.

Los archivos maestros (`memoria.tex` y `memoria.lyx`) contienen información sobre como modificar parámetros básicos (autor, título, fecha, etc.).

###Bibliografía
La bibliografía está contenida en el archivo `memoria.bib`, y los estilos están en el archivo `usm.bst` (que básicamente es una castellanización de las normas APA, *American Psychological Association*).

**Nota:** El mayor problema suele originarse al tratar de incluir nuevas referencias web o publicaciones en línea (del tipo **webpage**).

Por favor, revise los ejemplos incluidos para asegurarse de que tengan el mismo formato y descriptores (*url, howpublished, author, year*, etc.). Si al compilar ve referencias extrañas en lugar de los nombres de los autores de artículos citados, entonces lo más probable es que tenga un problema con una de sus referencias del tipo **webpage**.

##Contribuciones

Dirigirlas por correo electrónico directamente al autor (o vía *PayPal* si alguien se siente generoso) ![wink](http://www.csell.net/2010/04/09/developersmackdown-com-stickers/wlEmoticon-wink_2.png).

##Licencia

> Educational Community License version 1.0 (Se Adjunta)

Básicamente dice que puede ocuparse para fines educacionales como usted quiera, pero que las **modificaciones al trabajo original** deben ser distribuidas de manera que no generen confusión respecto del **trabajo original** o de su autor.

**Nota:** Las imágenes son propiedad intelectual de la Universidad Técnica Federico Santa María, y están protegidas por leyes chilenas e internacionales de derechos de autor.