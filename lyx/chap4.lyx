#LyX 2.0 created this file. For more info see http://www.lyx.org/
\lyxformat 413
\begin_document
\begin_header
\textclass book
\use_default_options true
\master memoria.lyx
\maintain_unincluded_children false
\language spanish
\language_package default
\inputencoding auto
\fontencoding global
\font_roman default
\font_sans default
\font_typewriter default
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100

\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref false
\papersize default
\use_geometry false
\use_amsmath 1
\use_esint 1
\use_mhchem 1
\use_mathdots 1
\cite_engine natbib_authoryear
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\use_refstyle 1
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Chapter
Instalación de LaTeX y LyX
\end_layout

\begin_layout Section
LaTeX
\end_layout

\begin_layout Standard
LaTeX es un sistema de preparación de documentos de alta calidad visual
 
\begin_inset CommandInset citation
LatexCommand citep
key "latex:whatis"

\end_inset

.
 Si no ha ocupado LaTeX anteriormente, visite esta página:
\end_layout

\begin_layout Itemize
\begin_inset CommandInset href
LatexCommand href
target "http://www.latex-project.org/"

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Float figure
placement H
wide false
sideways false
status open

\begin_layout Plain Layout
\align center
\begin_inset Graphics
	filename figures/fig_latex_project_org.png
	lyxscale 60
	width 60text%

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption

\begin_layout Plain Layout
LaTeX Project
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
Puede obtener, en forma gratuita, las distribuciones de LaTeX, según su
 plataforma, en:
\end_layout

\begin_layout Itemize
Windows: 
\begin_inset CommandInset href
LatexCommand href
name "http://miktex.org/"
target "http://miktex.org/"

\end_inset

; también puede ocupar 
\begin_inset CommandInset href
LatexCommand href
name "http://www.tug.org/protext/"
target "http://www.tug.org/protext/"

\end_inset

.
\end_layout

\begin_layout Itemize
MacOS: 
\begin_inset CommandInset href
LatexCommand href
name "http://www.tug.org/mactex/"
target "http://www.tug.org/mactex/"

\end_inset

.
\end_layout

\begin_layout Itemize
Unix/Linux: 
\begin_inset CommandInset href
LatexCommand href
name "http://www.tug.org/texlive/"
target "http://www.tug.org/texlive/"

\end_inset

.
\end_layout

\begin_layout Standard
Para una referencia completa sobre LaTeX, recomendamos el libro de 
\begin_inset CommandInset citation
LatexCommand citealp
key "Lamport94"

\end_inset

; aunque para solucionar problemas específicos, su mejor aliado es Internet.
 Otros libros que puede consultar se presentan en la Bibliografía 
\begin_inset CommandInset citation
LatexCommand citep
key "Mittelbach04,Oetiker06,Roberts05"

\end_inset

.
\end_layout

\begin_layout Section
LyX
\end_layout

\begin_layout Standard
LyX es un procesador de texto que combina la potencia de LaTeX con las simplicid
ad de un editor gráfico (WYSIWYM) 
\begin_inset CommandInset citation
LatexCommand citep
key "lyx"

\end_inset

.
 Puede obtener LyX desde el siguiente enlace:
\end_layout

\begin_layout Itemize
\begin_inset CommandInset href
LatexCommand href
name "http://www.lyx.org/"
target "http://www.lyx.org/"

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Float figure
placement H
wide false
sideways false
status open

\begin_layout Plain Layout
\align center
\begin_inset Graphics
	filename figures/fig_lyx_org.png
	lyxscale 60
	width 60text%

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption

\begin_layout Plain Layout
Página Principal LyX
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
LyX es un sólo un editor, no podrá crear documentos en formato PDF (o DVI,
 PS, etc.) si no tiene LaTeX instalado en su sistema.
\end_layout

\begin_layout Standard
La última versión de LyX (con la que este documento fue creado es 2.0.5.1).
\end_layout

\begin_layout Subsection
Wiki LyX
\end_layout

\begin_layout Standard
El sistema de ayuda y documentación de LyX es muy amplio y es recomendable
 revisarlo si no está familiarizado con este sistema de preparación de documento
s 
\begin_inset CommandInset citation
LatexCommand citep
before "Revisar "
key "wikilyx"

\end_inset

.
\end_layout

\begin_layout Standard
\begin_inset Float figure
placement H
wide false
sideways false
status open

\begin_layout Plain Layout
\align center
\begin_inset Graphics
	filename figures/fig_wikilyx.png
	lyxscale 60
	width 60text%

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption

\begin_layout Plain Layout
Página Principal WikiLyX
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\end_body
\end_document
