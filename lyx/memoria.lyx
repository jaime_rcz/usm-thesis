#LyX 2.1 created this file. For more info see http://www.lyx.org/
\lyxformat 474
\begin_document
\begin_header
\textclass book
\begin_preamble
\usepackage{../memoriaUSM}

%---------------------------------------------------------------------------
%%% WATERMARK
% \usepackage{graphicx} %Required, if not loaded already!
\usepackage{type1cm}
\usepackage{eso-pic}
\usepackage{color}
\makeatletter
\AddToShipoutPicture{%
            \setlength{\@tempdimb}{.34\paperwidth}%
            \setlength{\@tempdimc}{.6\paperheight}%
            \setlength{\unitlength}{1pt}%
            \put(\strip@pt\@tempdimb,\strip@pt\@tempdimc){%
        %\makebox(0,0){\rotatebox{45}{\textcolor[gray]{0.9}%
        %{\fontsize{6cm}{6cm}\selectfont{DRAFT}}}}%
            \includegraphics{figures/logousm_watermark.jpg}
            }%
}
%---------------------------------------------------------------------------

%---------------------------------------------------------------------------
%%% CHAPTER HEADINGS
%
\usepackage{titlesec}
\titleformat{\chapter}[hang]{\Huge\bfseries}{\thechapter\hsp\textcolor{gray80}{|}\hsp}{0pt}{\Huge\bfseries}
%---------------------------------------------------------------------------

\usepackage{blindtext}
\end_preamble
\options oneside
\use_default_options true
\maintain_unincluded_children false
\language spanish
\language_package default
\inputencoding auto
\fontencoding global
\font_roman default
\font_sans default
\font_typewriter default
\font_math auto
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize 11
\spacing onehalf
\use_hyperref true
\pdf_title "Memoria Departamento de Industrias USM"
\pdf_author "Departamento de Industrias USM"
\pdf_subject "Memoria Departamento de Industrias USM"
\pdf_keywords "Memoria"
\pdf_bookmarks true
\pdf_bookmarksnumbered false
\pdf_bookmarksopen false
\pdf_bookmarksopenlevel 1
\pdf_breaklinks true
\pdf_pdfborder true
\pdf_colorlinks true
\pdf_backref section
\pdf_pdfusetitle true
\pdf_quoted_options "pdfinfo={producer={JCR's Template, http://www.rubin-de-celis.com/}},linkcolor=blue,urlcolor=blue,citecolor=blue"
\papersize default
\use_geometry false
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 0
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 0
\use_package mhchem 1
\use_package stackrel 0
\use_package stmaryrd 0
\use_package undertilde 0
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 1
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 2
\tocdepth 2
\paragraph_separation indent
\paragraph_indentation default
\quotes_language english
\papercolumns 1
\papersides 2
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Standard
\begin_inset Note Note
status open

\begin_layout Plain Layout
%%%%%%%%%%%%%%%%%%%%
\end_layout

\begin_layout Plain Layout
% Plantilla para Memorias y Tesis, USM
\end_layout

\begin_layout Plain Layout
% ============================== 
\end_layout

\begin_layout Plain Layout
% AUTOR: 
\end_layout

\begin_layout Plain Layout
% Jaime C.
 Rubin-de-Celis <jaime@rubin-de-celis.com> 
\end_layout

\begin_layout Plain Layout
% Departamento de Industrias - USM 
\end_layout

\begin_layout Plain Layout
% 
\end_layout

\begin_layout Plain Layout
% FECHA: $Date: 2015-02-01 00:04:12 -0400$ 
\end_layout

\begin_layout Plain Layout
% 
\end_layout

\begin_layout Plain Layout
% LICENCIA: 
\end_layout

\begin_layout Plain Layout
% Copyright (c) 2012-2015, Jaime C.
 Rubin-de-Celis 
\end_layout

\begin_layout Plain Layout
% Licensed under the Educational Community License version 1.0 (SE ADJUNTA)
 
\end_layout

\begin_layout Plain Layout
% (Se excluyen las imágenes propiedad de la USM) 
\end_layout

\begin_layout Plain Layout
%
\end_layout

\begin_layout Plain Layout
% %%%%%%%%%%%%%%%%%%%%
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout

%%% CONFIGURACIÓN %%%
\end_layout

\begin_layout Plain Layout


\backslash
newcommand{
\backslash
TheTitleOne}		{TÍTULO DE MEMORIA}
\end_layout

\begin_layout Plain Layout


\backslash
newcommand{
\backslash
TheTitleTwo}		{TÍTULO DE MEMORIA (LÍNEA 2)}
\end_layout

\begin_layout Plain Layout


\backslash
newcommand{
\backslash
TheTitleThree}		{TÍTULO DE MEMORIA (LÍNEA 3) [Opcional]}
\end_layout

\begin_layout Plain Layout


\backslash
newcommand{
\backslash
TheAuthor}		{NOMBRES Y APELLIDOS DEL AUTOR} 
\end_layout

\begin_layout Plain Layout


\backslash
newcommand{
\backslash
TheGrade}		{INGENIERO CIVIL INDUSTRIAL}
\end_layout

\begin_layout Plain Layout


\backslash
newcommand{
\backslash
TheAdvisor}		{SRA.
 XXXXXXX XXXXXXXX X.}
\end_layout

\begin_layout Plain Layout


\backslash
newcommand{
\backslash
TheCoAdvisor}		{SR.
 XXXXXXX XXXXXXXX X.}
\end_layout

\begin_layout Plain Layout

%
\backslash
newcommand{
\backslash
TheScndCoAdvisor}		{SRTA.
 XXXXXXX XXXXXXXX X.}  % Opcional (Borrar '%' del inicio de línea para activar).
\end_layout

\begin_layout Plain Layout


\backslash
newcommand{
\backslash
TheCity}		{SANTIAGO}
\end_layout

\begin_layout Plain Layout


\backslash
newcommand{
\backslash
TheDate}		{MAYO 2015}
\end_layout

\begin_layout Plain Layout

%
\end_layout

\begin_layout Plain Layout

%%% ====== NO EDITAR ADELANTE ====== %%%
\end_layout

\begin_layout Plain Layout


\backslash
pagestyle{plain}
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
frontmatter
\end_layout

\end_inset


\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
thispagestyle{empty}
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset CommandInset include
LatexCommand input
filename "portada.lyx"

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Newpage newpage
\end_inset


\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
thispagestyle{empty}
\end_layout

\end_inset


\end_layout

\begin_layout Section*
Agradecimientos (título es opcional)
\end_layout

\begin_layout Standard
\begin_inset CommandInset include
LatexCommand input
filename "agradecimientos.lyx"

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Newpage newpage
\end_inset


\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
thispagestyle{empty}
\end_layout

\end_inset


\begin_inset Phantom VPhantom
status open

\begin_layout Plain Layout

\end_layout

\end_inset


\begin_inset VSpace 4cm
\end_inset


\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
hfill
\end_layout

\end_inset


\begin_inset Box Frameless
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "50line%"
special "none"
height "1in"
height_special "totalheight"
status open

\begin_layout Plain Layout

\emph on
\begin_inset CommandInset include
LatexCommand input
filename "dedicatoria.lyx"

\end_inset


\end_layout

\end_inset


\begin_inset Newpage newpage
\end_inset


\end_layout

\begin_layout Section*
Resumen Ejecutivo
\end_layout

\begin_layout Standard
\begin_inset CommandInset include
LatexCommand input
filename "resumen.lyx"

\end_inset


\begin_inset Newpage newpage
\end_inset


\end_layout

\begin_layout Section*
Abstract
\end_layout

\begin_layout Standard
\begin_inset CommandInset include
LatexCommand input
filename "abstract.lyx"

\end_inset


\begin_inset Newpage newpage
\end_inset


\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
begin{spacing}{1}
\end_layout

\end_inset


\begin_inset CommandInset toc
LatexCommand tableofcontents

\end_inset


\end_layout

\begin_layout Standard
\begin_inset FloatList figure

\end_inset


\end_layout

\begin_layout Standard
\begin_inset FloatList table

\end_inset


\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
end{spacing}
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Note Note
status open

\begin_layout Plain Layout
Cuerpo: Capítulos que componen el cuerpo principal de la memoria.
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
mainmatter
\end_layout

\end_inset


\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
pagestyle{fancy}
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset CommandInset include
LatexCommand input
filename "chap1.lyx"

\end_inset


\end_layout

\begin_layout Standard
\begin_inset CommandInset include
LatexCommand input
filename "chap2.lyx"

\end_inset


\end_layout

\begin_layout Standard
\begin_inset CommandInset include
LatexCommand input
filename "chap3.lyx"

\end_inset


\end_layout

\begin_layout Standard
\begin_inset CommandInset include
LatexCommand input
filename "chap4.lyx"

\end_inset


\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout

%%% === BORRAR === %%%
\end_layout

\begin_layout Plain Layout


\backslash
blinddocument
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
begin{spacing}{1}
\end_layout

\end_inset


\begin_inset CommandInset bibtex
LatexCommand bibtex
bibfiles "memoria"
options "usm"

\end_inset


\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
end{spacing}
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\start_of_appendix
\begin_inset CommandInset include
LatexCommand input
filename "anexo.lyx"

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Note Note
status open

\begin_layout Plain Layout
Bibliografía: Editar el archivo memoria.bib con un editor de texto o con
 un editor bibliográfico.
\end_layout

\end_inset


\end_layout

\end_body
\end_document
